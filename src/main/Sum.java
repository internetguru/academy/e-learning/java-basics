import java.util.Scanner;

public class Sum {

  /**
   * Read two integers from input and print their sum.
   */
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int number1 = input.nextInt();
    int number2 = input.nextInt();
    input.close();
    System.out.println(number1 + number2);
  }

}
