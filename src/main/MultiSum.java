import java.util.Scanner;

public class MultiSum {

  /**
   * Read all integers from input and print their sum.
   */
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int sum = 0;
    while (input.hasNextInt()) {
      int number = input.nextInt();
      sum += number;
    }
    input.close();
    System.out.println(sum);
  }

}
